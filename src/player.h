#ifndef __PLAYER__
#define __PLAYER__

#include "controls.h"
#include "rectangle.h"
#include "shot.h"

#define P_WIDTH 9
#define P_HEIGHT 10

typedef struct player{
    rectangle *position;
    unsigned short health;
    controls *controls;
    shot *shot;
} player;

//Cria o player e o insere no board
player* create_player(unsigned short scale, unsigned short health,
        unsigned short x, unsigned short y,
        unsigned short max_x, unsigned short max_y);

//Gerencia a vida do player
void update_health(player *player, int delta);

//Retorna a vida do player
int get_health(player *player);

void player_shoot(unsigned short scale, player *player,
        unsigned short max_x, unsigned short max_y);

void delete_player(player *player);
#endif
