#ifndef __ENEMY__
#define __ENEMY__

#include "rectangle.h"
#include "shot.h"

typedef struct enemy{
    rectangle *position;
    enum {WEAK, MEDIUM, STRONG} type;
} enemy;

enemy* create_enemy(int type,
        unsigned short scale, 
        unsigned short x, unsigned short y, 
        unsigned short max_x, unsigned short max_y);

shot *enemy_shoot(unsigned short scale, enemy *enemy,
        unsigned short max_x, unsigned short max_y);

void delete_enemy(enemy *enemy);

#endif
