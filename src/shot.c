#include <stdlib.h>
#include <stdio.h>
#include "shot.h"

shotlist* create_shotlist(){
    shotlist *nlist;

    nlist = malloc(sizeof(shotlist));
    nlist->first = NULL;

    return nlist;
}

void delete_shotlist(shotlist *list){
    shot *index = list->first, *aux;

    while(index){
        aux = index;
        index = index->next;
        delete_shot(aux);
    }

    free(list);
}

shot* create_shot(unsigned short scale,
        unsigned short x, unsigned short y,
        unsigned short max_x, unsigned short max_y,
        unsigned short damage, unsigned short trajectory){

    int width, height;
    shot *nshot;

    switch (damage) {
        case 1:
            width  = 1;
            height = 6;
            break;
        case 2:
            width = 3;
            height = 7;
            break;
        case 3:
            width = height = 5;
            break;

    
    }

    if(trajectory > 1)
        return NULL;

    nshot = malloc(sizeof(shot));
    if(!nshot)
        return NULL;

    nshot->trajectory = trajectory;
    nshot->damage = damage;
    nshot->position = create_rectangle(width*scale, height*scale, x, y, max_x, max_y);
    nshot->next = NULL;

    return nshot;
}

int insert_shot(shotlist *list, shot *insrt){
    shot *aux = list->first;

    if(!aux){
        list->first = insrt;
        return 1;
    }

    while(aux->next != NULL)
        aux = aux->next;

    aux->next = insrt;
    return 1;
}

int remove_shot(shotlist *list, shot *rm){
    shot *aux = list->first;
    if(!aux)
        return 0;

    if(aux == rm){
        list->first = rm->next;
        delete_shot(rm);
        return 1;
    }

    while(aux->next != NULL && aux->next != rm)
        aux = aux->next;

    //Nao achou rm
    if(!aux->next)
        return 0;

    aux->next = rm->next;
    delete_shot(rm);
    return 1;
}

void delete_shot(shot *shot){
    delete_rectangle(shot->position);
    free(shot);
}
void move_shot(shot *shot){

    if(shot && !shot->trajectory)
        shot->position->y += 8;
    else
        shot->position->y -= 8;
}
