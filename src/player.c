#include <stdio.h>
#include <stdlib.h>
#include "player.h"

player* create_player(unsigned short scale, unsigned short health,
        unsigned short x, unsigned short y, 
        unsigned short max_x, unsigned short max_y){

    player *nplayer;

    if(!(nplayer = malloc(sizeof(player))))
        return NULL;

    nplayer->controls =  create_controls();
    nplayer->health = health;
    nplayer->shot = 0;

    nplayer->position = create_rectangle(P_WIDTH*scale, P_HEIGHT*scale, x, y, max_x, max_y);
    if(!nplayer->position)
        return NULL;

    return nplayer;
}

void update_health(player *player, int delta){
    player->health += delta;
}

int get_health(player *player){
    return player->health;
}

void player_shoot(unsigned short scale, player *player, unsigned short max_x, unsigned short max_y){

    if(player->shot)
        return;

    player->shot = create_shot(scale, player->position->x, player->position->y,
            max_x, max_y, 1, 1);
    if(!player->shot)
        printf("create_shot fail\n");
}

void delete_player(player *player){

    delete_rectangle(player->position);
    delete_controls(player->controls);
    if(player->shot)
        delete_shot(player->shot);
    free(player);
}
