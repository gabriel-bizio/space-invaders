#ifndef __CONTROLS__
#define __CONTROLS__

typedef struct {
    unsigned char right;
    unsigned char left;
    unsigned char up;
} controls;

controls* create_controls();
void delete_controls(controls *element);
void controls_left(controls *element);
void controls_right(controls *element);
void controls_up(controls *element);

#endif
