#include <stdlib.h>
#include "controls.h"

controls* create_controls(){
    controls *ncontrols = malloc(sizeof(controls));
    if(!ncontrols)
        return NULL;
    
    ncontrols->right = 0;
    ncontrols->left = 0;
    ncontrols->up = 0;

    return ncontrols;
}

void delete_controls(controls *element){ free(element);}

void controls_left(controls *element){ element->left = element->left ^ 1;}

void controls_right(controls *element){ element->right = element->right ^ 1;}

void controls_up(controls *element){ element->up = element->up ^ 1;}
