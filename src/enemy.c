#include <stdlib.h>
#include <stdio.h>

#include "enemy.h"

enemy* create_enemy(int type,
        unsigned short scale,
        unsigned short x, unsigned short y, 
        unsigned short max_x, unsigned short max_y){

    unsigned short width, height;
    enemy *nenemy;
    if(!(nenemy = malloc(sizeof(enemy))))
        return NULL;

    if(type < 0 || type > 3 )
        nenemy->type = 0;
    else
        nenemy->type = type;

    switch (type) {
        case WEAK:
            width = 11;
            height = 8;
            break;
        case MEDIUM:
            width = 11;
            height = 9;
            break;
        case STRONG:
              width = 8;
              height = 8;
              break;
    }

    nenemy->position = create_rectangle(width*scale, height*scale, x, y, max_x, max_y);

    return nenemy;
}

shot *enemy_shoot(unsigned short scale, enemy *enemy, unsigned short max_x, unsigned short max_y){
    int damage;

    switch (enemy->type) {

        case WEAK:
            damage = 1;
            break;
        case MEDIUM:
            damage = 2;
            break;
        case STRONG:
            damage = 3;
            break;
    }
    return create_shot(scale, enemy->position->x, enemy->position->y, max_x, max_y, damage, 0);
}

void delete_enemy(enemy *enemy){
    delete_rectangle(enemy->position);
    free(enemy);
}
