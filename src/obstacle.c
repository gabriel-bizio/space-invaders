#include <stdlib.h>
#include "obstacle.h"

obstacle *create_obstacle(unsigned short scale,
            unsigned short x, unsigned short y,
            unsigned short max_x, unsigned short max_y){

    obstacle *n_obstacle = malloc(sizeof(obstacle));
    if(!n_obstacle)
        return NULL;
    
    n_obstacle->position = create_rectangle(O_WIDTH*scale, O_HEIGHT*scale, x, y, max_x, max_y);
    n_obstacle->health = 10;

    return n_obstacle;
}

void delete_obstacle(obstacle *obstacle){
    delete_rectangle(obstacle->position);
    free(obstacle);
}
