#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>

#include <allegro5/bitmap.h>
#include <allegro5/bitmap_draw.h>
#include <allegro5/bitmap_io.h>
#include <allegro5/color.h>
#include <allegro5/drawing.h>
#include <allegro5/allegro5.h>
#include <allegro5/allegro_font.h> 
#include <allegro5/allegro_ttf.h>
#include <allegro5/allegro_primitives.h>
#include <allegro5/allegro_image.h>
#include <allegro5/display.h> 
#include <allegro5/events.h>
#include <allegro5/keyboard.h>
#include <allegro5/keycodes.h>
#include <allegro5/system.h>
#include <allegro5/timer.h>

#include "enemy.h"
#include "obstacle.h"
#include "player.h"
#include "controls.h"
#include "shot.h"
#include "rectangle.h"


#define X_SCREEN 960
#define Y_SCREEN 540

#define ENEMY_ROWS 5
#define ENEMY_COLUMNS 11
#define OBSTACLES 4

#define SCALE 3

#define MYSTERY_SHIP_TIMER 800
#define SHIELD_SPAWN_TIMER 1400
#define SHIELD_ACTIVE_TIMER 900
//
//Manage (m) functions
void m_enemy_spawn(enemy *enemies[ENEMY_ROWS][ENEMY_COLUMNS], int *n_enemies);
void m_enemy_shots(enemy *enemies[ENEMY_ROWS][ENEMY_COLUMNS], shotlist *enemy_shots);
void m_enemy_movement(enemy *enemies[ENEMY_ROWS][ENEMY_COLUMNS], int *strafe_count, int *down);

//Manage renders
void render_health(int health, rectangle *health_bar, ALLEGRO_BITMAP *texture);
void render_shot(shot *shot, ALLEGRO_BITMAP *texture);
void render_enemy(enemy *enemy, ALLEGRO_BITMAP *texture, ALLEGRO_TIMER *timer);

unsigned char collision_2D(rectangle *element1, rectangle *element2){

    if ((
        ((element2->y-element2->height/2 >= element1->y-element1->height/2) && 
         (element1->y+element1->height/2 >= element2->y-element2->height/2)) ||
        ((element1->y-element1->height/2 >= element2->y-element2->height/2) &&
         (element2->y+element2->height/2 >= element1->y-element1->height/2))) &&
        (((element2->x-element2->width/2 >= element1->x-element1->width/2) &&
          (element1->x+element1->width/2 >= element2->x-element2->width/2)) ||
        ((element1->x-element1->width/2 >= element2->x-element2->width/2) &&
         (element2->x+element2->width/2 >= element1->x-element1->width/2))))
        return 1;

    return 0;
}

void render_rectangle(rectangle *rectangle, ALLEGRO_BITMAP *texture, float sx, float sy){

    if(!rectangle){
        printf("Rectangle is null\n");
        return;
    }

    al_draw_scaled_bitmap(texture, sx, sy, rectangle->width/SCALE+1, rectangle->height/SCALE+1,
                    rectangle->x - rectangle->width/2,
                    rectangle->y - rectangle->height/2,
                    rectangle->width,
                    rectangle->height,
                    0);
}

void update_position(player *player){

    if(player->controls->right)
        move_rectangle(player->position, 1, 1, X_SCREEN, Y_SCREEN);

    if(player->controls->left)
        move_rectangle(player->position, 1, 0, X_SCREEN, Y_SCREEN);

    if(player->controls->up)
        player_shoot(SCALE, player, X_SCREEN, Y_SCREEN);
}

int main(){
    int i, j;
    int score = 0, n_enemies = 0;
    int strafe_count, down;

    int player_hit = 0;
    int shield_active = 0;

    int player_hit_timer = 60;
    int enemy_hit_timer = 60;
    int mystery_ship_timer = MYSTERY_SHIP_TIMER;
    int shield_spawn_timer = SHIELD_SPAWN_TIMER;
    int shield_active_timer = SHIELD_ACTIVE_TIMER;

    player *player = create_player(SCALE, 2, X_SCREEN/2, Y_SCREEN-30, X_SCREEN, Y_SCREEN);
    enemy *enemies[ENEMY_ROWS][ENEMY_COLUMNS];
    enemy *enemy;

    srand(time(NULL));

    rectangle *explosion = 0;
    rectangle *health_bar = create_rectangle(SCALE*30, SCALE*9, X_SCREEN-60, 30,
            X_SCREEN, Y_SCREEN);

    rectangle *shield = 0;
    rectangle *shield_buffer;

    rectangle *mystery_ship = 0;
    rectangle *ship_buffer;

    shotlist *enemy_shots = create_shotlist();
    shot *shot, *index;

    obstacle *obstacles[OBSTACLES];
    obstacle *obstacle;

    for(i=0; i<OBSTACLES; i++)
        obstacles[i] = create_obstacle(SCALE, 260+i*150, Y_SCREEN-100, X_SCREEN, Y_SCREEN);

    al_init();
    al_init_image_addon();
    al_init_primitives_addon();
    al_init_ttf_addon();
    al_install_keyboard();


    ALLEGRO_TIMER *timer = al_create_timer(1.0/60.0);
    ALLEGRO_EVENT_QUEUE *queue = al_create_event_queue();
    ALLEGRO_FONT *title = al_load_ttf_font("../textures/title.ttf", 100, 0);
    ALLEGRO_FONT *interface = al_load_ttf_font("../textures/arcade/retro-pixel-arcade.ttf", 15, 0);
    ALLEGRO_DISPLAY *disp = al_create_display(X_SCREEN, Y_SCREEN);
    ALLEGRO_BITMAP *texture = al_load_bitmap("../textures/SpaceInvaders.bmp");
    ALLEGRO_BITMAP *health = al_load_bitmap("../textures/SpaceInvaders_Health.png");
    ALLEGRO_EVENT event;

    al_register_event_source(queue, al_get_keyboard_event_source()); al_register_event_source(queue, al_get_display_event_source(disp)); al_register_event_source(queue, al_get_timer_event_source(timer));
    al_start_timer(timer);

    while(1){
        al_wait_for_event(queue, &event);

        if(event.type == 30){
            al_draw_textf(title, al_map_rgb(0, 255, 0), X_SCREEN/8, 20, 0, "SPACE  INVADERS");

            al_draw_text(interface, al_map_rgb(255, 255, 255), (X_SCREEN/3)-20, Y_SCREEN/3, 0,
                    "Seta para cima - atirar");
            al_draw_text(interface, al_map_rgb(255, 255, 255), (X_SCREEN/3)-20, (Y_SCREEN/3)+25, 0,
                    "Setas laterais - andar");
            al_draw_text(interface, al_map_rgb(255, 255, 255), (X_SCREEN/3)-20, (Y_SCREEN/3)+50, 0,
                    "Espaco - jogar");

            al_flip_display();
        }
        else if(event.type == 10 || event.type == 12){
            if(event.keyboard.keycode == ALLEGRO_KEY_SPACE)
                break;
        }
        else if(event.type == 42)
            return 1;
    }

    while(1){


        if(!n_enemies){
            m_enemy_spawn(enemies, &n_enemies);

            strafe_count = 20;
            down = 0;
            if(player->health<5)
                player->health++;
        }


        al_wait_for_event(queue, &event);
        if(event.type == 30){
            al_clear_to_color(al_map_rgb(0, 0, 0));
            al_draw_textf(interface, al_map_rgb(255, 255, 255), 20, 20, 0, "SCORE: %d", score);
            al_draw_textf(interface, al_map_rgb(255, 255, 255), X_SCREEN-200, 20, 0, "LIVES: ");
            render_health(player->health, health_bar, health);

            if(shield_active){
                al_draw_textf(interface, al_map_rgb(255, 255, 255), 20, Y_SCREEN-20, 0, "SHIELD");
                shield_active_timer--;
                if(!shield_active_timer)
                    shield_active = 0;
            }

            update_position(player);
            render_rectangle(player->position, texture, 68, 3);

            if(explosion){
                if(al_get_timer_count(timer)%60 <= 30){
                    render_rectangle(explosion, texture, 34, 52);
                }
                else
                    render_rectangle(explosion, texture, 33, 67);

                if(!player_hit_timer){
                    player_hit = 0;
                    player_hit_timer = 60;
                    delete_rectangle(explosion);
                    explosion = 0;
                }

                if(!enemy_hit_timer){
                    enemy_hit_timer = 60;
                    delete_rectangle(explosion);
                    explosion = 0;
                }

                if(player_hit){
                    if(shield_active)
                        explosion->x = player->position->x;
                    else
                        player->position->x = explosion->x;
                    player_hit_timer--;
                }
                else
                    enemy_hit_timer--;
            }

            if(player_hit){
                if(!explosion){
                    explosion = create_rectangle(15*SCALE, 12*SCALE, player->position->x,
                            player->position->y, X_SCREEN, Y_SCREEN);
                }
                else



                player_hit_timer--;
            }

            //Nave do player;

            //Tiro do player
            if(player->shot){

                move_shot(player->shot);
                render_shot(player->shot, texture);
                if(player->shot->position->y > Y_SCREEN){
                    delete_shot(player->shot);
                    player->shot = 0;
                }
            }
            if(!get_health(player))
                break;

            //Tiros inimigos

             if(!(al_get_timer_count(timer)%120))
                m_enemy_shots(enemies, enemy_shots);

            //Colisoes de tiros inimigos
            index = enemy_shots->first;
            while(index){
                move_shot(index);
                render_shot(index, texture);

                shot = 0;
                //Se o tiro saiu do mapa
                if(index->position->y > Y_SCREEN) 
                    shot = index;
                //Colisao tiro do inimigo -> tiro do player
                else if(player->shot && collision_2D(player->shot->position, index->position)){
                    shot = player->shot;
                    player->shot = 0;
                    delete_shot(shot);

                    shot = index;
                }
                //Colisao tiro do inimigo -> player
                else if(collision_2D(player->position, index->position)){
                    shot = index;
                    if(!shield_active)
                        player->health--;
                    player_hit = 1;
                    player->position->x = X_SCREEN/2; 
                }
                //Colisao tiro inimigo -> obstaculo
                else{
                    for(i=0; i<OBSTACLES; i++){
                        if(obstacles[i] && collision_2D(index->position, obstacles[i]->position)){
                            shot = index;
                            obstacles[i]->health--;
                        }
                    }
                }

                index = index->next;
                if(shot)
                    remove_shot(enemy_shots, shot);
            }

            //Movimento dos inimigos
            if(al_get_timer_count(timer)%60 == 0)
                m_enemy_movement(enemies, &strafe_count, &down);

            //Render e colisao dos inimigos
            for(i=0; i<ENEMY_ROWS; i++){
                for(j=0; j<ENEMY_COLUMNS; j++){
                    if(!enemies[i][j])
                        continue;

                    //Animacao e render das texturas
                    enemy = enemies[i][j];
                    render_enemy(enemy, texture, timer);

                    //Colisao tiro do player -> inimigo
                    if(player->shot && collision_2D(player->shot->position, enemy->position)){
                        shot = player->shot;
                        enemies[i][j] = 0;
                        player->shot = 0;

                        score += 10+(10*enemy->type);
                        if(!explosion)
                            explosion = create_rectangle(15*SCALE, 12*SCALE, enemy->position->x,
                                enemy->position->y, X_SCREEN, Y_SCREEN);
                        else{
                            explosion->x = enemy->position->x;
                            explosion->y = enemy->position->y;
                        }

                        enemy_hit_timer = 60;

                        delete_enemy(enemy);
                        delete_shot(shot);
                        n_enemies--;
                        continue;
                    }
                }
            }

            //Render e colisao dos obstaculos
            for(i=0; i<OBSTACLES; i++){
                if(!obstacles[i])
                    continue;
                obstacle = obstacles[i];
                if(obstacle->health <= 0){
                    obstacles[i] = 0;
                    delete_obstacle(obstacle);
                    continue;
                }

                render_rectangle(obstacle->position, texture, 51, 20);

                //Colisao tiro do player -> obstaculo
                if(player->shot && collision_2D(player->shot->position, obstacle->position)){
                    shot = player->shot;
                    player->shot = 0;
                    if(shot->damage > 1)
                        obstacle->health -= 2;
                    else
                     obstacle->health--;;

                    delete_shot(shot);
                }
            }

            //Nave bonus
            if(!mystery_ship_timer && !mystery_ship){
                mystery_ship = create_rectangle(16*SCALE, 8*SCALE, X_SCREEN-(16*SCALE), 30,
                        X_SCREEN, Y_SCREEN);
                mystery_ship_timer = MYSTERY_SHIP_TIMER; 
            }
            if(mystery_ship){
                if(!(al_get_timer_count(timer)%4))
                    move_rectangle(mystery_ship, 1, 0, X_SCREEN, Y_SCREEN);
                render_rectangle(mystery_ship, texture, 49, 5);

                if(player->shot && collision_2D(mystery_ship, player->shot->position)){
                    score+=200;

                    ship_buffer = mystery_ship;
                    mystery_ship = 0;
                    delete_rectangle(ship_buffer);

                    shot = player->shot;
                    player->shot = 0;
                    delete_shot(shot);
                }

                if(mystery_ship && mystery_ship->x <= (mystery_ship->width/2)+10){
                    ship_buffer = mystery_ship;
                    mystery_ship = 0;
                    delete_rectangle(ship_buffer);
                }
            }
            else
                mystery_ship_timer--;

            //Poder bonus: Escudo
            if(!shield_spawn_timer && !shield){
                shield = create_rectangle(8*SCALE, 8*SCALE, X_SCREEN-(16*SCALE), 30,
                        X_SCREEN, Y_SCREEN);
                shield_spawn_timer = SHIELD_SPAWN_TIMER;
            }
            if(shield){
                if(!(al_get_timer_count(timer)%4))
                    move_rectangle(shield, 1, 0, X_SCREEN, Y_SCREEN);
                render_rectangle(shield, texture, 4, 52);

                if(player->shot && collision_2D(shield, player->shot->position)){
                    shield_buffer = shield;
                    shield = 0;
                    delete_rectangle(shield_buffer);

                    shield_active = 1;
                    shield_active_timer = SHIELD_ACTIVE_TIMER;

                    shot = player->shot;
                    player->shot = 0;
                    delete_shot(shot);
                }

                if(shield && shield->x <= (shield->width/2)+10){
                    shield_buffer = shield;
                    shield = 0;
                    delete_rectangle(shield_buffer);
                }
            }
            else
                shield_spawn_timer--;

            al_flip_display();
        }
        else if((event.type == 10) || (event.type == 12)){
            switch(event.keyboard.keycode){

                case ALLEGRO_KEY_UP:
                    controls_up(player->controls);
                    break;
                case ALLEGRO_KEY_LEFT:
                    controls_left(player->controls);
                    break;
                case ALLEGRO_KEY_RIGHT:
                    controls_right(player->controls);
                    break; 
                default:
                    continue;
            }
        }
        else if(event.type == 42) break;
    }

    delete_shotlist(enemy_shots);

    if(mystery_ship)
        delete_rectangle(mystery_ship);

    if(shield)
        delete_rectangle(shield);

    delete_rectangle(health_bar);
    if(player->shot)
        delete_shot(player->shot);
    delete_player(player);
    for(i=0; i<ENEMY_ROWS; i++){
        for(j=0; j<ENEMY_COLUMNS; j++){
            if(enemies[i][j])
                delete_enemy(enemies[i][j]);
        }
    }

    for(i=0; i<OBSTACLES; i++){
        if(obstacles[i])
            delete_obstacle(obstacles[i]);
    }

    al_destroy_bitmap(texture);
    al_destroy_bitmap(health);
    al_destroy_font(interface);
    al_destroy_font(title);
    al_destroy_display(disp);
    al_destroy_timer(timer);
    al_destroy_event_queue(queue);

    al_uninstall_keyboard();
    al_shutdown_ttf_addon();
    al_shutdown_primitives_addon();
    al_shutdown_image_addon();

    return 0;
}

void m_enemy_spawn(enemy *enemies[ENEMY_ROWS][ENEMY_COLUMNS], int *n_enemies){
    int i, j;

    *n_enemies = ENEMY_ROWS*ENEMY_COLUMNS;

    for(i=0; i<ENEMY_ROWS; i++){
        for(j=0; j<ENEMY_COLUMNS; j++){
            if(!i)
                enemies[i][j] = create_enemy(STRONG, SCALE, 260+(j+1)*40, 50+(i+1)*40,
                        X_SCREEN, Y_SCREEN);
            else if(i == 1 || i == 2)
                enemies[i][j] = create_enemy(MEDIUM, SCALE, 260+(j+1)*40, 50+(i+1)*40,
                        X_SCREEN, Y_SCREEN);
            else
                enemies[i][j] = create_enemy(WEAK, SCALE, 260+(j+1)*40, 50+(i+1)*40,
                        X_SCREEN, Y_SCREEN);
        }
    }
}
void m_enemy_shots(enemy *enemies[ENEMY_ROWS][ENEMY_COLUMNS], shotlist *enemy_shots){
    int i, can_shoot;
    int coord[2];
    enemy *enemy;
    shot *index, *shot;

    for(i=0; i<2; i++){
        do{
            coord[0] = rand()%5;
            coord[1] = rand()%11;
        }
        while(!enemies[coord[0]][coord[1]]);
        enemy = enemies[coord[0]][coord[1]];

        can_shoot = 1;
        switch (enemy->type){
            case WEAK:
                if((coord[0] == ENEMY_ROWS-2) && (enemies[ENEMY_ROWS-1][coord[1]])){
                    can_shoot = 0;
                    break;
                }
            case MEDIUM:
                for(index=enemy_shots->first; index!=NULL; index=index->next){
                    if(index->position->y == enemy->position->y)
                        can_shoot = 0;
                    break;
                }
            case STRONG:
                break;
        }

        if(can_shoot){
            shot = enemy_shoot(SCALE, enemy, X_SCREEN, Y_SCREEN);
            insert_shot(enemy_shots, shot);
        }
    }
}

void m_enemy_movement(enemy *enemies[ENEMY_ROWS][ENEMY_COLUMNS], int *strafe_count, int *down){
    int i, j; 
    enemy *enemy;

    for(i=0; i<ENEMY_ROWS; i++){
        for(j=0; j<ENEMY_COLUMNS; j++){
            if(!enemies[i][j])
                continue;

            enemy = enemies[i][j];
            if(!(*down))
                move_rectangle(enemy->position, 1, 1, X_SCREEN, Y_SCREEN);
            else
                move_rectangle(enemy->position, 1, 0,  X_SCREEN, Y_SCREEN);

            if(!(*strafe_count))
                enemy->position->y += 20;
        }
    }
    if(!(*strafe_count)){
        if((*down))
            (*down) = 0;
        else
            (*down) = 1;
        (*strafe_count) = 40;
    }
    else
        (*strafe_count)--;
}

void render_health(int health, rectangle *health_bar, ALLEGRO_BITMAP *texture){
    int y;

    y = 3+16*(5-health);
    render_rectangle(health_bar, texture, 1, y);
}

void render_shot(shot *shot, ALLEGRO_BITMAP *texture){
    int x, y;

    switch(shot->damage){
        case 1:
            x = 39;
            y = 5;
            break;
        case 2:
            x = 38;
            y = 21;
            break;
        case 3:
            x = 37;
            y = 38;
            break;
    }

    render_rectangle(shot->position, texture, x, y);
}

void render_enemy(enemy *enemy, ALLEGRO_BITMAP *texture, ALLEGRO_TIMER *timer){

    switch (enemy->type) {
        case WEAK:
            if(al_get_timer_count(timer)%60 <= 30)
                render_rectangle(enemy->position, texture, 3, 35);
            else
                render_rectangle(enemy->position, texture, 19, 35);
            break;

        case MEDIUM:
            if(al_get_timer_count(timer)%60 <= 30)
                render_rectangle(enemy->position, texture, 3, 3);
            else
                render_rectangle(enemy->position, texture, 19, 3);
            break;

        case STRONG:
            if(al_get_timer_count(timer)%60 <= 30)
                render_rectangle(enemy->position, texture, 4, 19);
            else
                render_rectangle(enemy->position, texture, 20, 19);
            break;
    }
}
