#ifndef __OBSTACLE__
#define __OBSTACLE__

#include "rectangle.h"

#define O_WIDTH 26
#define O_HEIGHT 12

typedef struct obstacle{
    rectangle *position;
    int health;
}obstacle;

obstacle *create_obstacle(unsigned short scale,
        unsigned short x, unsigned short y,
        unsigned short max_x, unsigned short max_y);

void delete_obstacle(obstacle *obstacle);
#endif
