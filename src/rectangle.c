#include <stdlib.h>
#include "rectangle.h"

rectangle* create_rectangle(unsigned char width, unsigned short height,
        unsigned short x, unsigned short y,
        unsigned short max_x, unsigned short max_y){

    if ((x - width/2 < 0) || (x + width/2 > max_x) || (y - height/2 < 0) || (y + height/2 > max_y)) return NULL;

    rectangle *new_rectangle = malloc(sizeof(rectangle));
    if(!new_rectangle)
        return NULL;

    new_rectangle->width = width;
    new_rectangle->height = height;
    new_rectangle->x = x;
    new_rectangle->y = y;
    
    return new_rectangle;
}

void move_rectangle(rectangle *element, char steps, unsigned char trajectory, unsigned short max_x, unsigned short max_y){
    if (!trajectory){if ((element->x - steps*RECTANGLE_STEP) - element->width/2 >= 0) element->x = element->x - steps*RECTANGLE_STEP;}
    else if (trajectory == 1){if ((element->x + steps*RECTANGLE_STEP) + element->width/2 <= max_x) element->x = element->x + steps*RECTANGLE_STEP;}
    else if (trajectory == 2){if ((element->y - steps*RECTANGLE_STEP) - element->height/2 >= 0) element->y = element->y - steps*RECTANGLE_STEP;}
    else if (trajectory == 3){if ((element->y + steps*RECTANGLE_STEP) + element->height/2 <= max_y) element->y = element->y + steps*RECTANGLE_STEP;}
}

void delete_rectangle(rectangle *element){
    free(element);
}
