#ifndef __SQUARE__
#define __SQUARE__

#define RECTANGLE_STEP 10

typedef struct {
    unsigned char width;
    unsigned char height;
    unsigned short x;
    unsigned short y;
} rectangle;

rectangle* create_rectangle(
        unsigned char width,
        unsigned short height,
        unsigned short x,
        unsigned short y,
        unsigned short max_x,
        unsigned short max_y);

void move_rectangle(rectangle *element,
        char steps,
        unsigned char trajectory,
        unsigned short max_x,
        unsigned short max_y);

void delete_rectangle(rectangle *element);

#endif
