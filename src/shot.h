#ifndef __SHOT__
#define __SHOT__

#include "rectangle.h"

typedef struct shot{
    rectangle *position;
    unsigned short trajectory;
    unsigned short damage;
    struct shot *next;
} shot;

typedef struct shotlist{
    shot *first;
} shotlist;

shotlist* create_shotlist();

void delete_shotlist(shotlist *list);

shot *create_shot(unsigned short scale,
        unsigned short x, unsigned short y,
        unsigned short max_x, unsigned short max_y,
        unsigned short damage, unsigned short trajectory);

int insert_shot(shotlist *list, shot *insrt);

int remove_shot(shotlist *list, shot *rm);

void delete_shot(shot *shot);

void move_shot(shot *shot);

#endif
