# Space Invaders

## Particularidades

As regras dp jogo foram implementadas seguindo o site [classicgaming.cc](http://www.classicgaming.cc/classics/space-invaders/play-space-invaders)
assim, os diferentes inimigos valem:

- Fracos - 10 pontos.
- Médios - 20 pontos.
- Fortes - 30 pontos. 

Percebe-se que o inimigo forte garante apenas 30 pontos ao inves de 40. Fiz isso para deixar as regras do jogo mais 
consistentes com as outras especificadas no site.

Fora isso, a unica diferença entre essa entrega e as especificações, é que os inimigos mais próximos 
do player não possuem nenhuma prioridade para atirar (inclui um arquivo backup com a implementaçao disso, porém nao acho que funcione bem)


## Funcionalidades adicionais implementadas

- Inimigos se movem, em bloco, para a direita e para a esquerda
- Projéteis da nave podem se chocar com os dos inimigos e se anularem
- Inserção de colunas vazias entre os inimigos
- Nave inimiga (originalmente vermelha, branca neste trabalho) com pontuação extra atrás de todos os inimigos
- Implementação de um poder bônus (escudo) temporário para a nave

